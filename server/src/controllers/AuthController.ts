import { z } from "zod";
import { prisma } from "../lib/prisma";
import { FastifyReply, FastifyRequest } from "fastify";
import { verifyToken } from "../api/GoogleAuth";
export class AuthController {

  async sign_up(req: FastifyRequest, rep: FastifyReply) {

    const body = z.object({
      name: z.string().trim(),
      email: z.string().email(),
      avatar_url: z.string().url(),
      password: z.string().min(4),
      confirmPassword: z.string().min(4),
    }).superRefine(({ confirmPassword, password }, ctx) => {
      if (confirmPassword !== password) {
        ctx.addIssue({
          code: "custom",
          message: "The passwords did not match"
        });
      }
    });

    const { name, email, avatar_url } = body.parse(req.body);

    try {
      const userEmailAlreadyExists = await prisma.user.findFirst({
        where: { email: email.toString().trim() }
      });

      if (userEmailAlreadyExists) {
        return rep.status(401).send({ message: 'Email already exists' });
      }

      await prisma.user.create({
        data: {
          name: name,
          email: email,
          avatar_url: avatar_url
        }
      });

    } catch (error) {
      return rep.status(500).send({ "message": "Internal server error" });
    }
  }

  async sign_in(req: FastifyRequest, rep: FastifyReply) {

    const body = z.object({
      email: z.string().email(),
      password: z.string().trim(),
    });

    const { email, password } = body.parse(req.body);

    try {
  
      let user = await prisma.user.findFirst({
        where: { email: email }
      });

      if (!user) {
        return rep.status(401).send({ message: 'The email address or password is incorrect. Please retry...' });
      }

      return rep.jwtSign({
        id: user.id,
        name: user.name,
        email: user.email
      });
    } catch (error) {
      console.log(error);
      return rep.status(500).send({ message: "internal server error" });
    }
  }
}