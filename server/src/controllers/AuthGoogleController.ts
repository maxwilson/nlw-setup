import { z } from "zod";
import { prisma } from "../lib/prisma";
import { FastifyReply, FastifyRequest } from "fastify";
import { verifyToken } from "../api/GoogleAuth";

export class AuthGoogleControler {
  
  async sign_in(req: FastifyRequest, rep: FastifyReply) {

    const body = z.object({
      token: z.string().trim(),
    });

    const { token } = body.parse(req.body);

    try {
      
      const userGoogle = await verifyToken(token);
     
      if (!userGoogle) return rep.status(500).send({ "message": "failed to authenticate" });

      let user = await prisma.user.findFirst({
        where: { sub: userGoogle.sub }
      });

      if (!user) {
        user = await prisma.user.create({
          data: {
            sub: userGoogle.sub,
            name: `${userGoogle.name}`,
            email: userGoogle.email,
            avatar_url: userGoogle.picture
          }
        });
      }

      const tokenJwt = await rep.jwtSign({
        id: user.id,
        name: user.name,
        email: user.email
      });

      return rep.status(200).send({ 
        token: tokenJwt,
        user : {
          name : user.name,
          email : user.email,
          picture : user.avatar_url
        }
      });
    } catch (error) {
      console.log(error);
      return rep.status(500).send({ message: "internal server error" });
    }
  }
}