import { FastifyRequest, FastifyReply } from 'fastify'
import dayjs from "dayjs";
import { z } from "zod";
import { prisma } from "../lib/prisma";



export class HabitController {

  async create(req: FastifyRequest, rep: FastifyReply) {
    const createHabitBody = z.object({
      title: z.string(),
      weekDays: z.array(z.number().min(0).max(6)),
    });

    const { title, weekDays } = createHabitBody.parse(req.body);

    const today = dayjs().startOf('day').toDate();

    try {
      await prisma.habit.create({
        data: {
          title,
          user_id: req.user.id,
          created_at: today,
          weekDays: {
            create: weekDays.map(day => {
              return {
                week_day: day,
              }
            })
          }
        }
      });

    } catch (error) {
      return rep.status(500).send({ "mensagem": "Internal server error" });
    }
  }

  async dayHabitList(req: FastifyRequest, rep: FastifyReply) {
    const getDayParams = z.object({
      date: z.coerce.date(),
    });

    const { date } = getDayParams.parse(req.query);

    const parseDate = dayjs(date).startOf('day');

    const weekDay = parseDate.get('day');

    console.log(parseDate.toDate())

    try {

      const possibleHabits = await prisma.habit.findMany({
        where: {
          user_id: req.user.id,
          created_at: {
            lte: date,
          },
          weekDays: {
            some: {
              week_day: weekDay
            }
          }
        }
      });

      const day = await prisma.day.findFirst({
        where: {
          date: parseDate.toDate(),
        },
        include: {
          dayHabits: {
            where : {
              user_id : req.user.id,
            }
          },
        }
      });

      const completedHabits = day?.dayHabits.map(dayHabit => {
        return dayHabit.habit_id
      }) ?? [];

      return {
        possibleHabits,
        completedHabits
      }

    } catch (error) {
      return rep.status(500).send({ "mensagem": "Internal server error" });
    }
  }

  async habitsToggle(req: FastifyRequest, rep: FastifyReply) {
    const toggleHabitParams = z.object({
      id: z.string().uuid()
    });

    const { id } = toggleHabitParams.parse(req.params);

    const today = dayjs().startOf('day').toDate();

    try {

      let day = await prisma.day.findFirst({
        where: {
          date: today,
        }
      });

      if (!day) {
        day = await prisma.day.create({
          data: {
            date: today,
          }
        });
      }

      const dayHabit = await prisma.dayHabit.findUnique({
        where: {
          day_id_habit_id: {
            day_id: day.id,
            habit_id: id,
          }
        }
      });

      if (dayHabit) {
        //remover a marcação de completo

        await prisma.dayHabit.delete({
          where: {
            id: dayHabit.id,
          }
        });


      } else {
        await prisma.dayHabit.create({
          data: {
            day_id: day.id,
            habit_id: id,
            user_id: req.user.id
          }
        });
      }

    } catch (error) {
      return rep.status(500).send({ "message": "Internal server error" });
    }
  }

  async summaryList(req: FastifyRequest, rep: FastifyReply) {

    try {
      //Query mais complexa, mais condições, relacionamentos => QSL na mão (RAW)
      //Prisma ORM: Raw SQL => SQLITE
      const summary = await prisma.$queryRaw`
        SELECT
          D.id,
          D.date,
          (
            SELECT
              cast(count(*) as float)
            FROM day_habits DH
            WHERE DH.day_id = D.id AND DH.user_id = ${req.user.id}
          ) as completed,
          (
            SELECT
              cast(count(*) as float)
            FROM habit_week_days HWD
            JOIN habits H
              ON H.id = HWD.habit_id
            WHERE 
              H.user_id = ${req.user.id} AND
              HWD.week_day = cast(strftime('%w', D.date/1000.0, 'unixepoch') as int)
              AND H.created_at <= D.date
          ) as amount
        FROM days D
        
        WHERE 
          EXISTS( SELECT habits.user_id FROM habits WHERE habits.user_id = ${req.user.id} )
      `;
      console.log(summary)
      return summary;
    } catch (error) {
      console.log(error)
      return rep.status(500).send({ "message": "Internal server error" })
    }
  }

}