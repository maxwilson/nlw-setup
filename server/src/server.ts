import fastify from "fastify";
import cors from '@fastify/cors';
import { appRoutes } from "./routes";
import fastifyJwt from "@fastify/jwt";
import "./lib/dotEnv";
import decorateAuthenticate from "./middleware/decorateAuthenticate";

const app = fastify();

app.register(cors);
app.register(fastifyJwt, { secret: process.env.JWT_SECRET_KEY! });
app.register(decorateAuthenticate);
app.register(appRoutes);

const PORT = 3333;

app.listen({
  port: PORT,
  host: '0.0.0.0'
}).then(() => {
  console.log(`HTTP Server running on http://localhost:${PORT}`);
});