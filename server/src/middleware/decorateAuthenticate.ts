import { FastifyRequest, FastifyReply } from "fastify";
import fp from "fastify-plugin";

export default fp(async function(fastify, opts) {
  fastify.decorate("verifyTokenJwt", async function(request: FastifyRequest, reply: FastifyReply) {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  })
});
