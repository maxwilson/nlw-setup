import { FastifyInstance } from 'fastify';

export const verifyJwtToken =async ( app: FastifyInstance) => {
  app.addHook("onRequest", async (request, reply) => {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  });
};