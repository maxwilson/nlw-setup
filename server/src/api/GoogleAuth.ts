
import axios from 'axios';

interface ResponseUserProps {
  sub: string;
  name: string;
  given_name: string;
  family_name: string;
  picture: string;
  email: string;
  email_verified: boolean;
  locale: string;
}

export async function verifyToken(token: string): Promise<ResponseUserProps | null> {
  try {

    const { data } = await axios.get("https://www.googleapis.com/oauth2/v3/userinfo", {
      headers: {
        "Authorization": `Bearer ${token}`
      }
    }).catch(err => {
      console.log(err);
      throw new Error(err);
    });

    let user: ResponseUserProps = data;

    return user;
  } catch (error) {
    console.log(error);
    throw new Error('failed to verify token');
  }
}
