import fastify from "fastify"

declare module 'fastify' {
  interface FastifyInstance {
    verifyTokenJwt: () => FastifyInstance
  }
}