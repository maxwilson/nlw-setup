import { FastifyInstance } from 'fastify'

import { HabitController } from './controllers/HabitController';
import { AuthController } from './controllers/AuthController';
import { AuthGoogleControler } from './controllers/AuthGoogleController';

export async function appRoutes(app: FastifyInstance) {

  app.post('/sign-up', new AuthController().sign_up);
  app.post('/oauth2/auth/sign-in', new AuthGoogleControler().sign_in);
 
  app.post("/habits", { onRequest: [app.verifyTokenJwt] }, new HabitController().create);

  app.get("/day", { onRequest: [app.verifyTokenJwt] }, new HabitController().dayHabitList);

  //completar / não-completar um hábito
  app.patch('/habits/:id/toggle',{ onRequest: [app.verifyTokenJwt] }, new HabitController().habitsToggle);

  app.get('/summary',{ onRequest: [app.verifyTokenJwt] }, new HabitController().summaryList);

}

