import axios from 'axios';
import { env } from './viteEnv';

const BASE_URL = env.VITE_API_BASEURL;

export const api = axios.create({
  baseURL: BASE_URL,
});