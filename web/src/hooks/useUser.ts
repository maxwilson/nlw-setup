import { useContext, useState } from 'react';
import { useLocalStorage } from './useLocalStorage';
import { AuthContext } from '../context/AuthContext';

export interface UserProps {
  name: string;
  email: string;
  token?: string;
  picture?: string;
}

export const useUser = () => {
  //const [user, setUser] = useState<UserProps | null>(null);
  const { user, setUser } = useContext(AuthContext);
  //const [user, setUser] = useState<UserProps | null>();
  const { setItem } = useLocalStorage();

  const addUser = (user: UserProps) => {
    setUser(user);
    setItem('user', JSON.stringify(user));
  };

  const removeUser = () => {
    setUser(null);
    setItem('user', '');
  };

  return { user, addUser, removeUser, setUser };
};