import { createContext } from 'react';
import { UserProps } from '../hooks/useUser';

interface AuthContext {
  user: UserProps | null;
  setUser: (user: UserProps | null) => void;
}

export const AuthContext = createContext<AuthContext>({
  user: null,
  setUser: () => {},
});

