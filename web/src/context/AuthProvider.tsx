import { createContext, useContext, useEffect, useState } from 'react';
import { UserProps } from '../hooks/useUser';
import { useLocalStorage } from '../hooks/useLocalStorage';

interface Props {
  children?: React.ReactNode;
}

interface AuthContextData {
  signed: boolean;
  user: UserProps | null;
  Login(user: UserProps): Promise<void>;
  Logout(): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider = ({ children }: Props ) => {
  const [user, setUser] = useState<UserProps | null>(null);
  const { getItem, setItem } = useLocalStorage();
  
  useEffect(() => {
    const user = getItem('user');
    if (user) {
      setUser(JSON.parse(user));
    }
  }, []);


  async function Login(user: UserProps) {
    setUser(user);
    setItem('user', JSON.stringify(user));
  }

  function Logout() {
    setUser(null);
  }
  
  return (
    <AuthContext.Provider
      value={{ signed: Boolean(user), user, Login, Logout }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);
  return context;
}