import { Navigate, Outlet } from "react-router-dom";

export function RequireAuth() {
  const user = localStorage.getItem('user');
  let token: string = '';

  if (user) {
    token = JSON.parse(user)?.token;
  }

  return (
    token ? <Outlet /> : <Navigate to="/sign-in" replace />
  );
}