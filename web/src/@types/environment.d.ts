interface ImportMeta {
  env: {
    VITE_API_BASEURL: string;
    VITE_NODE_ENV: 'development' | 'production';
    VITE_KEY_GOOGLE : string;
  };
}