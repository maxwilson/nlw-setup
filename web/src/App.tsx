import { AppRoutes } from './routes';
import './styles/global.css';
import './lib/dayjs';
//import { useAuth } from './hooks/useAuth';
//import { AuthContext } from './context/AuthContext';
import { AuthProvider } from './context/AuthProvider';

/*export function App() {
  const { user, setUser } = useAuth();
  return (
    <AuthContext.Provider value={{ user, setUser }}>
      <AppRoutes />
    </AuthContext.Provider>
  );
}*/

export function App() {
  return (
    <AuthProvider>
      <AppRoutes />
    </AuthProvider>
  );
}