import { useGoogleLogin } from '@react-oauth/google';
import logoImage from '../assets/logo.svg';
import { api } from '../lib/axios';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthProvider';

export function Sign_in() {
  const navigate = useNavigate();
  const { Login } = useAuth();

  const handlerGoogleLogin = useGoogleLogin({
    onSuccess: async ({ access_token }) => {
      try {

        // Test 
        await api.post("/oauth2/auth/sign-in", {
          token: access_token
        }).then(res => {

          Login({
            name: res.data.user.name,
            email: res.data.user.email,
            picture: res.data.user.picture,
            token: res.data.token
          });

          return navigate(`/`, { replace: true });
        }).catch(err => {
          console.error(err);
        });

      } catch (err) {
        console.log(err)
      }
    }
  });

  return (
    <div className="w-full h-full text-gray-800 antialiased px-4 py-6 justify-center sm:py-12 mt-32">
      <div className="relative py-3 sm:max-w-xl mx-auto text-center">
        <img src={logoImage} alt="habits" className="h-12 w-full" />

        <div className="relative mt-8 text-white shadow-md sm:rounded-lg text-left sm:max-w-xl">
          <div className="h-1 bg-violet-500 rounded-t-md"></div>

          <div className="py-6 px-8 flex flex-col gap-2 mt-3">

            <div className="flex justify-center items-center">
              <button onClick={() => handlerGoogleLogin()} className="bg-white text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center focus:outline-none focus:ring-2 focus:ring-violet-500 focus:ring-offset-2 focus:ring-offset-zinc-900">
                <svg className="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path fill="#fbc02d" d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12	s5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24s8.955,20,20,20	s20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"></path><path fill="#e53935" d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039	l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"></path><path fill="#4caf50" d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36	c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"></path><path fill="#1565c0" d="M43.611,20.083L43.595,20L42,20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571	c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"></path>
                </svg>
                <span className="font-semibold leading-tight">Faça login no Google</span>
              </button>
            </div>

            <div className="inline-flex items-center justify-center w-full h-full">
              <hr className="w-full h-px my-8 border-0 dark:bg-zinc-700" />
              <span className="absolute px-3 font-medium text-gray-900 -translate-x-1/2 bg-white left-1/2 dark:text-white dark:bg-gray-900">ou</span>
            </div>

            <label htmlFor="email" className="font-semibold leading-tight">Email</label>
            <input
              id="email"
              className="p-3 rounded-lg bg-zinc-800 text-white placeholder:text-zinc-400 focus:outline-none focus:ring-2 focus:ring-violet-500 focus:ring-offset-2 focus:ring-offset-zinc-900"
              type="text"
              placeholder="informe seu melhor e-mail"
            />

            <label htmlFor="password" className="mt-1 font-semibold leading-tight">Senha</label>
            <input
              id="password"
              className="p-3 rounded-lg bg-zinc-800 text-white placeholder:text-zinc-400 focus:outline-none focus:ring-2 focus:ring-violet-500 focus:ring-offset-2 focus:ring-offset-zinc-900"
              placeholder="Digite sua senha"
              type="Password"
            />

            <div className="flex justify-between items-baseline">
              <button className="text-white py-2 px-4 mt-4 rounded-lg p-3 flex items-center justify-center gap-3 font-semibold bg-green-600 hover:bg-green-500 transition-colors focus:outline-none focus:ring-2 focus:ring-green-600 focus:ring-offset-2 focus:ring-offset-zinc-900">Enviar</button>
              <a href="#" className="text-sm hover:underline">Esqueceu a senha?</a>
            </div>

          </div>

        </div>

      </div>
    </div>
  );
}