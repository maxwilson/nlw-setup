import { Routes, Route } from "react-router-dom";

import { Home } from "../pages/Home";
import { Sign_in } from "../pages/Sign_in";
import { RequireAuth } from "../components/RequireAuth";

export function AppRoutes() {
  return (
    <Routes>
      <Route>
        <Route path="/sign-in" element={<Sign_in />} />
        <Route element={<RequireAuth />}>
          <Route path="/*" element={<Home />} />
        </Route>
      </Route>
    </Routes>
  )
}
